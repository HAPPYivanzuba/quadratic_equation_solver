#include <iostream>
#include <math.h>
using namespace std;

void print_solution(float sol);

int main()
{
    float a;
    float b;
    float c;
    float D;
    float x1;
    float x2;
    int mnoh;
    int finish;
    setlocale(LC_ALL,"Rus");
    while(true)
    {
        cout<<"Введите a:"<<endl;
        cin>>a;
        cout<<"Введите b:"<<endl;
        cin>>b;
        cout << "Введите c:" << endl;
        cin>>c;
        if (a==0)
        {
        cout<<"ЭТО НЕ КВАДРАТНОЕ УРАВНЕНИЕ"<<endl;
        }
        else
        {
            D=b*b-4*a*c;
            if(D<0)
            {
                cout<<"Корней нет"<<endl;
            }
            else
            {
                D=sqrt(D);
                x1=(-b+D)/(2*a);
                cout<<"Первый вариант="<<x1<<endl;
                x2=(-b-D)/(2*a);
                cout<<"Второй вариант="<<x2<<endl;
                do
                {
                cout<<"Хотите увидеть разложение на множители, если да то введите 1,если нет введите 0";
                cin>>mnoh;
                }
                while(mnoh!=0 and mnoh!=1);
                if(mnoh==1)
                {
                    /*if(x1>=0 and x2>=0)
                    {
                    cout<<a<<" * "<<"(x-("<<x1<<")) * (x-("<<x2<<"))"<<endl;
                    }
                    if(x1<=0 and x2>=0)
                    {
                        x1=x1*-1;
                        cout<<a<<" * "<<"(x+("<<x1<<")) * (x-("<<x2<<"))"<<endl;
                    }
                    if(x2<=0 and x1>=0)
                    {
                        x2=x2*-1;
                        cout<<a<<" * "<<"(x-"<<x1<<") * (x+"<<x2<<")"<<endl;
                    }
                    if(x2<=0 and x1<=0)
                    {
                        x2=x2*-1;
                        x1=x1*-1;
                        cout<<a<<" * "<<"(x+"<<x1<<") * (x+"<<x2<<")"<<endl;
                    }*/
                    if (a!=1) cout << a << " * ";
                    print_solution(x1);
                    cout << " * ";
                    print_solution(x2);
                    cout << endl;
                }
            }
        }
        do
        {
            cout<<"Хотите еще? Если да, введите 1, если нет нажмите 0"<<endl;
            cin>>finish;
        }
        while (finish!=0 and finish!=1);
        if(finish==0)
        {
            return 0;
        }

    }
}

void print_solution(float sol)
{
    if (sol<0) cout << "(x+" << sol*-1 <<")";
    if (sol>0) cout << "(x-" << sol <<")";
    if (sol==0) cout << "(x)";
    return;
}
